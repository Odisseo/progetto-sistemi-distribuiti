package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ComputedData {
    double mean;
    double standard_dev;

    public ComputedData(){}

    public ComputedData(double m, double sd){
        mean=m;
        standard_dev=sd;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public double getStandard_dev() {
        return standard_dev;
    }

    public void setStandard_dev(double standard_dev) {
        this.standard_dev = standard_dev;
    }
    public String toString(){
        return "Media:"+mean+" Deviazione Standard:"+standard_dev;
    }
}
