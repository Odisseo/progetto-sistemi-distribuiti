package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Node_reference {

    String ip; int nn_port; int sens_port;
    Position position;
    int id;

    public Node_reference(){}

    public Node_reference(int sid, String sip, int com_p, int sens_p, Position p){
        id=sid;
        ip=sip;
        nn_port=com_p;
        sens_port=sens_p;
        position = p;
    }

    public void setId(int id){
        this.id=id;
    }

    public int getId(){
        return id;
    }


    public Position getPosition(){return position;}

    public void setPosition(Position position) {
        this.position = position;
    }
    public String toString(){
        return "id: "+id+"" +
                "\n\tPosition:"+position.getX()+" "+position.getY()+"\n" +
                "\tIP:"+this.ip+"   porta nodo-nodo:"+this.nn_port+"   porta nodo-sensore:"+this.sens_port;
    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getNn_port() {
        return nn_port;
    }

    public void setNn_port(int nn_port) {
        this.nn_port = nn_port;
    }

    public int getSens_port() {
        return sens_port;
    }

    public void setSens_port(int sens_port) {
        this.sens_port = sens_port;
    }
}
