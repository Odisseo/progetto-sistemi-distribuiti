package beans;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Node_reference_list {
    private List<Node_reference> list;

    public Node_reference_list(){}

    public Node_reference_list(List<Node_reference> ls){
        list=ls;
    }

    public List<Node_reference> getList() {
        return list;
    }

    public void setList(List<Node_reference> list) {
        this.list = list;
    }
}
