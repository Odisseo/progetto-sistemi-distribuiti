package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Position implements Comparable<Position>{
    int x=0,y=0;

    public Position(){
        x = (int) (Math.random()*100);
        y = (int) (Math.random()*100);
    }

    public Position(int ax, int ay){
        x=ax;
        y=ay;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int compareTo(Position o) {
        if(x==o.x && y==o.y)
            return 0;
        if(x>o.x)
            return 1;
        else
            return -1;
    }

    public int getDistance(Position o){
        return Math.abs(y-o.y)+Math.abs(x-o.x);
    }

    public static boolean isValid(Position o){
        return o.x<=100&&o.y<=100;
    }
}
