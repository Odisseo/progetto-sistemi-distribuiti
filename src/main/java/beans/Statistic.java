package beans;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class Statistic {
    long time;
    double value;
    int  node_id;

    public Statistic(){}

    public Statistic(int n,long t, double v){
        time=t;
        value=v;
        node_id=n;
    }
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getNode_id() {
        return node_id;
    }

    public void setNode_id(int node_id) {
        this.node_id = node_id;
    }

    public String toString(){
        return "(Node id = "+node_id+", "+toStringNoId()+")";
    }
    public String toStringNoId(){
        return "mean = "+value+", time = "+new Date(time);}
}
