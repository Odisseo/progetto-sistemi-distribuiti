package beans;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class Statistic_list {
    ArrayList<Statistic> data;
    public Statistic_list(){}

    public Statistic_list(ArrayList<Statistic> ls){
        data=ls;
    }


    public ArrayList<Statistic> getData() {
        return data;
    }

    public void setData(ArrayList<Statistic> data) {
        this.data = data;
    }
}
