package clients;

import beans.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class Analyst extends Thread{
    String rest_server_url;


    public Analyst(){
        rest_server_url = "http://localhost:1337/";
    }

    public Analyst(String ur){
        rest_server_url = ur;
    }
    public void run(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String read="";
        boolean keep_alive=true;
        try {

            while(keep_alive){
                System.out.println("Interfaccia Analista.\nComandi disponibili:\n" +
                        "* lista\n" +
                        "* st -> statistiche\n" +
                        "\tcittà <n>" +
                        "\n\tnodo <id> <n>" +
                        "\n* md -> media&deviazione\n" +
                        "\tcittà <n>" +
                        "\n\tnodo <id> <n>\n* esci\n>>");
                read=reader.readLine();
                StringTokenizer tokenizer = new StringTokenizer(read);
                String primo="";
                try {
                    primo = tokenizer.nextToken();
                }catch(NoSuchElementException e){}
                if(primo.equals("esci")){
                    keep_alive=false;
                }
                else if(primo.equals("lista")){
                    getFromServer("data/nodes",new ResponseNodesList());
                }
                else if(primo.equals("st")){
                    String secondo= tokenizer.nextToken();
                    if(secondo.equals("città")){
                        try {
                            int how_many = Integer.parseInt(tokenizer.nextToken());
                            System.out.println("Ultime " + how_many + "Statistiche tutta la città");
                            getFromServer("data/stat/city/" + how_many, new ResponseOutputList());
                        }catch(NoSuchElementException e){ }

                    }
                    else if(secondo.equals("nodo")){
                        try{
                            int node_id = Integer.parseInt(tokenizer.nextToken());
                            int how_many = Integer.parseInt(tokenizer.nextToken());
                            System.out.println("Le ultime "+how_many+"statistiche dal nodo "+node_id);
                            getFromServer("data/stat/node/"+node_id+"/"+how_many,new ResponseOutputList());
                        }catch(NoSuchElementException e){ }
                    }
                }else if(primo.equals("md")){
                    String secondo = tokenizer.nextToken();
                    if(secondo.equals("città")){
                        try{
                            int how_many = Integer.parseInt(tokenizer.nextToken());
                            System.out.println("media e deviazione tutta la città");
                            getFromServer("data/meansd/city/"+how_many,new ResponseMeanSd());
                        }catch(NoSuchElementException e){ }
                    }
                    else if(secondo.equals("nodo")){
                        try{
                            int node_id = Integer.parseInt(tokenizer.nextToken());
                            int how_many = Integer.parseInt(tokenizer.nextToken());
                            getFromServer("data/meansd/node/"+node_id+"/"+how_many,new ResponseMeanSd());
                        }catch(NoSuchElementException e){ }

                    }
                }

            }
            
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    public void getFromServer(String url_to_res, ResponseOutput strategy){
        Client cl = Client.create();
        WebResource resource=  cl.resource( rest_server_url+url_to_res);
        //System.out.println("DEBUG: "+rest_server_url+url_to_res);
        ClientResponse response = resource.accept("application/json").get(ClientResponse.class);

        if(response.getStatus()==200) {
            strategy.printServerResponse(response);
        }
        else if(response.getStatus()==404) {
            System.out.println("Elemento non trovato");
        }
        else{
            System.out.println("Risposta con errore " + response.getStatus());
        }

    }

    //a bit overkill
    private abstract class ResponseOutput{
        public abstract void  printServerResponse(ClientResponse r);
    }


    private class ResponseOutputList extends ResponseOutput{
        @Override
        public void printServerResponse(ClientResponse r) {
            Statistic_list ls = r.getEntity(Statistic_list.class);

            for(Statistic x: ls.getData()){
                System.out.println(x.toStringNoId());
            }
        }
    }

    private class ResponseNodesList extends ResponseOutput{
        @Override
        public void printServerResponse(ClientResponse r){
            Node_reference_list ls = r.getEntity(Node_reference_list.class);
            for(Node_reference x: ls.getList()){
                System.out.println(x);
            }
        }
    }

    private class ResponseMeanSd extends  ResponseOutput{

        @Override
        public void printServerResponse(ClientResponse r) {
            ComputedData data = r.getEntity(ComputedData.class);
            System.out.println(data);
        }
    }

}