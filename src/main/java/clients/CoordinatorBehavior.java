package clients;

import beans.Statistic;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import ems.EdgeMessageServiceOuterClass;
import util.DataCollector;

import java.util.*;

public class CoordinatorBehavior extends Thread{
    private String rest_url;
    private EdgeMessageServiceOuterClass.TimeMean last_global;
    private int last_global_mean_sum=0;
    private int last_global_numerosity=0;
    private Object global_mean_lock = new Object();
    private boolean keep_alive;
    private int new_index=0;
    private int old_index=0;
    private boolean data_present=false;
    private DataCollector data;

    public CoordinatorBehavior(String s){
        keep_alive=true;
        rest_url=s;
        data= new DataCollector();
        last_global = EdgeMessageServiceOuterClass.TimeMean.newBuilder().setMean(0).build();
    }

    @Override
    public void run(){
        while(keep_alive){
            if(data_present || data.isDataPresent()){
                long time = new Date().getTime();
                data_present=true;
                updateLastGlobalMean();
                sendGlobalDataToServer();
                sendNodesDataToServer();
                time=5000-(new Date().getTime()-time);
                if(time>0){
                    try {
                        Thread.sleep(time);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public void stopRunning(){
        keep_alive=false;
    }


    public EdgeMessageServiceOuterClass.TimeMean getLastGlobal(){
        EdgeMessageServiceOuterClass.TimeMean to_ret;
        synchronized (global_mean_lock){
            to_ret=last_global;
        }
        return  to_ret;
    }




    public void sendGlobalDataToServer(){
        if(getLastGlobal()!=null) {
            EdgeMessageServiceOuterClass.TimeMean t = getLastGlobal();
            Statistic statistic = new Statistic(0, t.getTime(), t.getMean());
            Client cl = Client.create();
            WebResource resource = cl.resource(rest_url + "data/stat/city");
            ClientResponse response = resource.type("application/json").accept("application/json").post(ClientResponse.class, statistic);
            if (response != null && response.getStatus() != 200) {
                System.out.println("Coordinatore a Server - dato globale: NO OK ");
            }
        }
    }

    public void addStatFromNode(Statistic s){
        boolean index_recorded=true;
        data.addValueTo(s);
        synchronized (global_mean_lock){
            last_global_numerosity++;
            last_global_mean_sum+=s.getValue();
        }
    }


    public void updateLastGlobalMean(){
        last_global = EdgeMessageServiceOuterClass.TimeMean.newBuilder()
                .setMean(last_global_mean_sum/last_global_numerosity)
                .setTime(Calendar.getInstance().getTimeInMillis()).build();
    }



    public void sendNodesDataToServer(){
        HashMap<Integer,ArrayList<Statistic>> dt = data.getSafeData();
        for(Integer x: dt.keySet()){

            List<Statistic> ls = dt.get(x);
            if(ls.size()>0) {
                Client cl = Client.create();
                WebResource resource = cl.resource(rest_url + "data/stat/node/" + x);
                //System.out.println("HERE-in");
                ClientResponse response;
                response = resource.type("application/json").accept("application/json").post(ClientResponse.class, ls);
                if (response.getStatus() != 200) {
                    System.out.println("Coordinatore a Server - dato nodo: NO OK: " + response.getStatus());
                }
                else {
                    data.removeElements(x,ls);
                }

            }
        }

        }



}
