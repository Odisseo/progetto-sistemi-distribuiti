package clients;

import beans.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import ems.EdgeMessageServiceGrpc;
import ems.EdgeMessageServiceOuterClass;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Node {
    private Node_reference reference;
    private boolean ongoing_election =false;
    private int coordinator_port = -1;
    private String coordinator_ip = "";
    private List<Node_reference> other_nodes;
    private String rest_url;
    private Server nn_server;
    private Server ns_server;
    private Object datalock = new Object();
    private Object daemonLock = new Object();
    private Object voter_lock = new Object();
    private Object other_nodes_lock = new Object();
    private Object ongoing_election_lock = new Object();
    private Object end_election = new Object();
    private Object others_election_lock = new Object();

    private DataDaemon dataDaemon = new DataDaemon();
    private ArrayList<Measurement> data = new ArrayList<>();
    private ArrayList<EdgeMessageServiceOuterClass.NodeMean> election_buffer = new ArrayList<>();
    private CoordinatorBehavior coordinator;
    private ElectionThread voter;
    private boolean isNodeReady=false;


    public Node(int id, String url, String self_ip, int com_port, int sen_port) {
        System.out.println("Creato il nodo " + id);
        rest_url = url;
        reference = new Node_reference(id, self_ip, com_port, sen_port, new Position((int) (Math.random()*100), (int) (Math.random()*100)));
    }

    public void start_running() {
        boolean isConnected = connect();

        if (isConnected) {
            startNodeGrpcServer();
            System.out.println("Lista di Nodi ricevuti:\n");
            for (Node_reference x : other_nodes)
                System.out.println(x.toString());
            helloNodes();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            isNodeReady = true;
            while (isNodeReady) {
                System.out.print("================INTERFACCIA NODO================" +
                        "\nt     ->termina informando il server di voler uscire dalla rete" +
                        "\ndata  ->Stampa riferimenti al nodo\n$ ");
                try {
                    String letta = reader.readLine();
                    if (letta.equals("t")) {
                        nn_server.shutdown();
                        ns_server.shutdown();
                        isNodeReady = false;
                        dataDaemon.keepUp = false;
                        synchronized (daemonLock) {
                            daemonLock.notify();
                        }
                        if (coordinator != null)
                            coordinator.stopRunning();
                        deleteMe();
                    } else if (letta.equals("data")) {
                        System.out.println(reference.toString());
                        System.out.println("Measurements: "+data.size()+"  Coordinator:"+coordinator_ip+":"+coordinator_port );
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            System.out.println("Termino");
        }
    }

    public boolean connect() {
        int attempt = 0;
        System.out.println("\tTentativo di connessione");
        Client cl = Client.create();
        WebResource resource = cl.resource(rest_url + "server/node");
        do {
            ClientResponse response;
            try {
                response = resource.type("application/json").accept("application/json").post(ClientResponse.class, reference);
            } catch (java.lang.AbstractMethodError e) {
                System.out.println("Comunicazione con il server non riuscita.");
                return false;
            }
            if (response.getStatus() == 200) {
                System.out.println("\t\tNodo accettato dal server in posizione " + reference.getPosition().getX() + " " + reference.getPosition().getY());
                Node_reference_list ls = response.getEntity(Node_reference_list.class);
                other_nodes = ls.getList();
                dataDaemon.start();
                return true;
            } else {
                attempt++;
                reference.setPosition(new Position());
                System.out.println("\t\tNodo rifiutato, nuova posizione: " + reference.getPosition().getX() + " " + reference.getPosition().getY() + " tentativo " + attempt + "/10");
            }
        } while (attempt < 10);

        return false;
    }

    public void deleteMe() {
        Client cl = Client.create();
        WebResource resource = cl.resource(rest_url + "server/node/" + this.getReference().getId());
        ClientResponse response = resource.delete(ClientResponse.class);
        System.out.println("Risposta del server: " + response.getStatus());


    }

    public void startNodeGrpcServer() {
        nn_server = ServerBuilder.forPort(reference.getNn_port()).addService(new EdgeMsgImpl()).build();
        ns_server = ServerBuilder.forPort(reference.getSens_port()).addService(new EdgeMsgImpl()).build();
        try {
            nn_server.start();
            ns_server.start();
            System.out.println("Server GRPC started!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void helloNodes() {
        System.out.println("Dimensione lista: "+other_nodes.size());
        for (int i = 0; i < other_nodes.size(); i++) {
            System.out.println("Chiedo al nodo "+i+" "+other_nodes.get(i).getId());

            String another_node_port = other_nodes.get(i).getIp() + ":" + other_nodes.get(i).getNn_port();
            final ManagedChannel channel = ManagedChannelBuilder.forTarget(another_node_port).usePlaintext(true).build();
            EdgeMessageServiceGrpc.EdgeMessageServiceBlockingStub stub = EdgeMessageServiceGrpc.newBlockingStub(channel);

            EdgeMessageServiceOuterClass.CallingCard richiesta = EdgeMessageServiceOuterClass.CallingCard.newBuilder()
                    .setId(reference.getId())
                    .setIp(reference.getIp())
                    .setPort(reference.getNn_port()).build();
            try {
                EdgeMessageServiceOuterClass.WhoIsCoordinator response = stub.requestCoordinator(richiesta);
                System.out.println("Risposta a richiesta coordinatore:\n\tIP coordinatore " + response.getIpCoordinatore() + "\n\tPortaCoordinatore " + response.getPortaCoordinatore());
                if(!response.getIpCoordinatore().equals("")) {
                    if(response.getIpCoordinatore().equals("elections")){
                       synchronized (others_election_lock) {
                           others_election_lock.wait(30000);
                       }
                    }
                    else {
                        coordinator_port = response.getPortaCoordinatore();
                        coordinator_ip = response.getIpCoordinatore();

                    }
                }

            } catch (io.grpc.StatusRuntimeException e) {
                channel.shutdown();
                System.out.println("Stato risposta dal nodo " + i + "/" + other_nodes.size() + ": " + e.getStatus().getCode().toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                channel.shutdown();
            }
        }

        if (coordinator_ip.equals("") && !isOngoing_election()) {
            coordinator_port = this.nn_server.getPort();
            coordinator_ip = this.reference.getIp();
            System.out.println("Sono il coordinatore");
            coordinator = new CoordinatorBehavior(rest_url);
            coordinator.start();
        }
    }


    public Node_reference getReference() {
        return reference;
    }

    public void setReference(Node_reference reference) {
        this.reference = reference;
    }

    public List<Node_reference> getOther_nodes() {
        return other_nodes;
    }

    public void setOther_nodes(List<Node_reference> other_nodes) {
        this.other_nodes = other_nodes;
    }

    public void printOnConsole(String s) {
        System.out.println(s);
    }

    public void propagateCoordinatorReference(){
        for(Node_reference x: other_nodes){
            final ManagedChannel channel = ManagedChannelBuilder.forTarget(x.getIp() + ":" + x.getNn_port()).usePlaintext(true).build();
            EdgeMessageServiceGrpc.EdgeMessageServiceStub stub = EdgeMessageServiceGrpc.newStub(channel);

            EdgeMessageServiceOuterClass.WhoIsCoordinator request = EdgeMessageServiceOuterClass.WhoIsCoordinator.newBuilder()
                    .setIpCoordinatore(reference.getIp())
                    .setPortaCoordinatore(reference.getNn_port())
                    .build();
            stub.wonElection(request, new StreamObserver<EdgeMessageServiceOuterClass.ElectionOk>() {
                @Override
                public void onNext(EdgeMessageServiceOuterClass.ElectionOk electionOk) {
                    onCompleted();
                }

                @Override
                public void onError(Throwable throwable) {
                    onCompleted();
                }

                @Override
                public void onCompleted() {
                    channel.shutdown();
                }
            });
            System.out.println("Mandata comunicazione di elezione vinta al nodo "+x.getId());

        }
    }

    public void safeAddNode(Node_reference n){
        synchronized (other_nodes_lock){
            List<Node_reference> ls ;
            ls = getOther_nodes();
            for(Node_reference x:other_nodes){
                if(x.getIp().equals(n.getIp()) && x.getNn_port()==n.getNn_port()){
                    System.out.println("Conoscevo già il nodo "+n.getId());
                    return;
                }
            }
            ls.add(n);
            System.out.println("ORA conosco il nodo "+n.getId());
        }
    }

    public void safeDeleteNode(String ip, int nn_port){
        synchronized (other_nodes_lock){
            Node_reference toRm=null;
            for(Node_reference x: other_nodes){
                if(x.getNn_port()==nn_port && x.getIp().equals(ip)){
                    toRm=x;
                    break;
                }
            }
            if(toRm!=null)
                other_nodes.remove(toRm);
        }
    }

    public boolean isOngoing_election(){
        boolean ret;
        synchronized (ongoing_election_lock){
            ret =  ongoing_election;
        }
        return ret;
    }

    public void setOngoing_election(boolean value){
        synchronized (ongoing_election_lock){
            ongoing_election=value;
        }
    }


    public void sendCoordinatorWhenReady(Node_reference n){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String targetIp = n.getIp()+":"+n.getNn_port();
                synchronized (end_election){
                    try {
                        end_election.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                final ManagedChannel channel = ManagedChannelBuilder.forTarget(targetIp).usePlaintext(true).build();
                EdgeMessageServiceGrpc.EdgeMessageServiceBlockingStub stub = EdgeMessageServiceGrpc.newBlockingStub(channel);
                EdgeMessageServiceOuterClass.WhoIsCoordinator richiesta = EdgeMessageServiceOuterClass.WhoIsCoordinator.newBuilder()
                        .setPortaCoordinatore(coordinator_port)
                        .setIpCoordinatore(coordinator_ip)
                        .build();
                EdgeMessageServiceOuterClass.Empty response = stub.sendCoordinator(richiesta);
                channel.shutdown();


            }
        };
        Thread t = new Thread(r);
        t.start();

    }

    private class EdgeMsgImpl extends EdgeMessageServiceGrpc.EdgeMessageServiceImplBase {

        @Override
        public void sendCoordinator(EdgeMessageServiceOuterClass.WhoIsCoordinator request, StreamObserver<EdgeMessageServiceOuterClass.Empty> responseObserver){
            coordinator_ip = request.getIpCoordinatore();
            coordinator_port = request.getPortaCoordinatore();
            synchronized (others_election_lock){
                others_election_lock.notify();
            }
            responseObserver.onNext(EdgeMessageServiceOuterClass.Empty.newBuilder().build());
            responseObserver.onCompleted();

        }

        @Override
        public void requestCoordinator(EdgeMessageServiceOuterClass.CallingCard request, StreamObserver<EdgeMessageServiceOuterClass.WhoIsCoordinator> responseObserver) {
            safeAddNode(new Node_reference(request.getId(),request.getIp(),request.getPort(),0,null));
            EdgeMessageServiceOuterClass.WhoIsCoordinator response;
            if(isOngoing_election()){
                response = EdgeMessageServiceOuterClass.WhoIsCoordinator.newBuilder()
                        .setIpCoordinatore("elections")
                        .setPortaCoordinatore(0).build();
                sendCoordinatorWhenReady(new Node_reference(request.getId(),request.getIp(),request.getPort(),0,null));

            }
            else{
                response = EdgeMessageServiceOuterClass.WhoIsCoordinator.newBuilder()
                        .setIpCoordinatore(coordinator_ip)
                        .setPortaCoordinatore(coordinator_port).build();
            }
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }

        @Override
        public void meanToCoordinator(EdgeMessageServiceOuterClass.NodeMean request, StreamObserver<EdgeMessageServiceOuterClass.TimeMean> responseObserver) {
            if (coordinator != null) {
                coordinator.addStatFromNode(new Statistic(request.getNodeId(), request.getData().getTime(), request.getData().getMean()));
                EdgeMessageServiceOuterClass.TimeMean response = coordinator.getLastGlobal();
                responseObserver.onNext(response);
            }
            responseObserver.onCompleted();
        }

        @Override
        public void sendMeasurement(EdgeMessageServiceOuterClass.MeasurementPayload request, StreamObserver<EdgeMessageServiceOuterClass.Empty> responseObserver) {
            EdgeMessageServiceOuterClass.Empty response = EdgeMessageServiceOuterClass.Empty.newBuilder().build();
            Measurement to_add = new Measurement(request.getId(), request.getType(), request.getValue(), request.getTimestamp());
            responseObserver.onNext(response);
            responseObserver.onCompleted();
            synchronized (datalock) {
                data.add(to_add);
            }
            synchronized (daemonLock) {
                daemonLock.notify();
            }

        }

        @Override
        public void startElection(EdgeMessageServiceOuterClass.ElectionPayload request, StreamObserver<EdgeMessageServiceOuterClass.ElectionOk> responseObserver){
            if(isNodeReady) {
                System.out.println("Ricevuto start election da " + request.getPartecipantId());
                int other = request.getPartecipantId();
                int error = 1;
                if (other < reference.getId())
                    error = 0;
                EdgeMessageServiceOuterClass.ElectionOk response = EdgeMessageServiceOuterClass.ElectionOk.newBuilder().setError(error).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
                if (!isOngoing_election())
                    setOngoing_election(true);
                synchronized (voter_lock) {
                    if (voter == null) {
                        voter = new ElectionThread();
                        voter.start();
                    }
                }
            }else{
                EdgeMessageServiceOuterClass.ElectionOk response = EdgeMessageServiceOuterClass.ElectionOk.newBuilder().setError(2).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
            }
        }

        @Override public void wonElection(EdgeMessageServiceOuterClass.WhoIsCoordinator request, StreamObserver<EdgeMessageServiceOuterClass.ElectionOk> responseObserver){
            coordinator_ip=request.getIpCoordinatore();
            coordinator_port=request.getPortaCoordinatore();
            setOngoing_election(false);
            synchronized (end_election) {
                end_election.notifyAll();
            }
            synchronized (voter_lock){
                if(voter!=null){
                    voter.interrupt();
                    voter=null;
                }
            }
            EdgeMessageServiceOuterClass.ElectionOk response = EdgeMessageServiceOuterClass.ElectionOk.newBuilder().build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }

    private class DataDaemon extends Thread  {
        boolean keepUp = true;


        @Override
        public void run()  {
            try {
                while (keepUp) {
                    int length;
                    synchronized (datalock) {
                        length = data.size();
                    }
                    if (length >= 40) {
                        ArrayList<Measurement> data_to_elaborate = new ArrayList<>();
                        synchronized (datalock) {
                            Collections.sort(data, new Comparator<Measurement>() {
                                @Override
                                public int compare(Measurement o1, Measurement o2) {
                                    return o1.compareTo(o2);
                                }
                            });
                            data_to_elaborate = new ArrayList<>(data.subList(0, 39));
                            data = new ArrayList<>(data.subList(20, data.size()));
                        }
                        double mean = 0;
                        for (Measurement x : data_to_elaborate)
                            mean += x.getValue();
                        mean /= data_to_elaborate.size();
                        long timestamp = Calendar.getInstance().getTimeInMillis();

                        final ManagedChannel channel = ManagedChannelBuilder.forTarget(coordinator_ip + ":" + coordinator_port).usePlaintext(true).build();
                        EdgeMessageServiceGrpc.EdgeMessageServiceBlockingStub stub = EdgeMessageServiceGrpc.newBlockingStub(channel);
                        EdgeMessageServiceOuterClass.NodeMean richiesta = EdgeMessageServiceOuterClass.NodeMean.newBuilder()
                                .setNodeId(reference.getId())
                                .setData(EdgeMessageServiceOuterClass.TimeMean.newBuilder()
                                        .setMean(mean)
                                        .setTime(timestamp))
                                .build();
                        if(!isOngoing_election()) {

                            try {
                                EdgeMessageServiceOuterClass.TimeMean response;
                                response = stub.meanToCoordinator(richiesta);

                                if(response.getTime()==0){
                                    System.out.println("Mandata Misurazione al coordinatore:"+mean+"  Statistica Globale non ancora calcolata");
                                }
                                else {
                                    System.out.println("Mandata Misurazione al coordinatore:" + mean + " Ultima statistica Globale :" + response.getMean() + " " + new Date(response.getTime()));
                                }
                                sendBufferedMeans();


                            } catch (io.grpc.StatusRuntimeException e) {
                                channel.shutdown();
                                System.out.println("Il coordinatore non risponde");
                                safeDeleteNode(coordinator_ip,coordinator_port);
                                setOngoing_election(true);
                                election_buffer.add(richiesta);
                                synchronized (voter_lock) {
                                    if (voter == null) {
                                        voter = new ElectionThread();
                                        voter.start();
                                    }
                                }
                            }
                        }
                        else{
                            election_buffer.add(richiesta);
                        }
                        channel.shutdown();

                    }
                    synchronized (daemonLock) {
                        daemonLock.wait();
                    }

                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void sendBufferedMeans(){
            for(EdgeMessageServiceOuterClass.NodeMean x: election_buffer){
                final ManagedChannel channel = ManagedChannelBuilder.forTarget(coordinator_ip + ":" + coordinator_port).usePlaintext(true).build();
                EdgeMessageServiceGrpc.EdgeMessageServiceStub stub = EdgeMessageServiceGrpc.newStub(channel);
                stub.meanToCoordinator(x, new StreamObserver<EdgeMessageServiceOuterClass.TimeMean>() {
                    @Override
                    public void onNext(EdgeMessageServiceOuterClass.TimeMean timeMean) {
                        onCompleted();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        onCompleted();
                    }

                    @Override
                    public void onCompleted() {
                        channel.shutdown();

                    }
                });
            }
            if(election_buffer.size()>0) {
                System.out.println("Mandate " + election_buffer.size() + " statistiche al coordinatore, raccolte durante l'elezione");
                election_buffer.clear();
            }
        }

    }

    private class ElectionThread extends Thread {
        Object received_lock = new Object();
        boolean received_ok;

        @Override
        public void run() {

            boolean elected=false;
            while(!elected){
                received_ok=false;
                printOnConsole("VOTER: inizio processo di elezione");
                ArrayList<Node_reference> higherId=new ArrayList<>();
                for(Node_reference x: other_nodes){
                    if(x.getId()>reference.getId())
                        higherId.add(x);
                }

                for(Node_reference x:higherId) {
                    sendStartElection(x);
                }

                try {
                    synchronized (received_lock) {
                        received_lock.wait(5000);
                    }

                    if (!received_ok) {
                        System.out.println("VOTER: Sono il nuovo Coordinatore");
                        coordinator_ip = reference.getIp();
                        coordinator_port = nn_server.getPort();
                        coordinator = new CoordinatorBehavior(rest_url);
                        coordinator.start();
                        elected=true;

                        propagateCoordinatorReference();
                        voter=null;
                    } else {
                        System.out.println("VOTER: ricevuto ok, mi metto in attesa di messagio dal nuovo coordinatore");
                        synchronized (end_election) {
                            end_election.wait(15000);
                        }
                        if(!isOngoing_election()){
                            elected=true;
                            voter=null;
                            System.out.println("VOTER: ricevuto messaggio di vittora dal nuovo Coordinatore");
                        }
                        else{
                            System.out.println("VOTER: NON RICEVUTO messaggio da nuovo coordinatore. Ricomincio le elezioni");
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        }

        public void sendStartElection(Node_reference n){

            final ManagedChannel channel = ManagedChannelBuilder.forTarget(n.getIp() + ":" + n.getNn_port()).usePlaintext(true).build();
            EdgeMessageServiceGrpc.EdgeMessageServiceStub stub = EdgeMessageServiceGrpc.newStub(channel);
            //System.out.println(reference.getId()+" mando start election a "+n.getId());
            EdgeMessageServiceOuterClass.ElectionPayload request = EdgeMessageServiceOuterClass.ElectionPayload.newBuilder()
                    .setPartecipantId(reference.getId())
                    .setPartecipantIp(reference.getIp())
                    .setPartecipantNnPort(reference.getNn_port()).build();
            stub.startElection(request, new StreamObserver<EdgeMessageServiceOuterClass.ElectionOk>() {
                @Override
                public void onNext(EdgeMessageServiceOuterClass.ElectionOk electionOk) {
                    if(electionOk.getError()==0)
                        synchronized (received_lock) {
                            received_ok = true;
                            received_lock.notify();
                        }
                    else if(electionOk.getError()==1){
                        System.out.println("Voter: Ho mandato messaggio di election ad un nodo avente id minore del mio");
                    }
                    else if(electionOk.getError()==2){
                        System.out.println("Voter: Ho mandato messaggio di election start ad un nodo non pronto");
                    }
                    onCompleted();
                }

                @Override
                public void onError(Throwable throwable) {
                    onCompleted();
                }

                @Override
                public void onCompleted() {
                    channel.shutdown();
                }
            });


        }


    }


}
