package clients;

import beans.Measurement;
import beans.Node_reference;
import beans.Position;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import ems.EdgeMessageServiceGrpc;
import ems.EdgeMessageServiceOuterClass;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import simulator.PM10Simulator;
import simulator.SensorStream;
import simulator.Simulator;

public class Sensor implements SensorStream {
    String rest_url;
    Position position;
    Node_reference node;
    NearNodeThread askThread;
    Simulator s;
    private Object lock = new Object();


    public Sensor(String u){
        rest_url=u;
        position = new Position();
        System.out.println("Sensore Generato in posizione "+position.getX()+" "+position.getY());
        askThread = new NearNodeThread(u,node);
        s  =new PM10Simulator(this);
        askThread.start();
        s.start();
    }

    public String getRest_url() {
        return rest_url;
    }

    public void setRest_url(String rest_url) {
        this.rest_url = rest_url;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public void sendMeasurement(Measurement m) {
        Node_reference ref_node;
        synchronized (lock) {
            ref_node=node;
        }

        if(ref_node!=null) {
            final ManagedChannel channel = ManagedChannelBuilder.forTarget(ref_node.getIp() + ":" + ref_node.getSens_port()).usePlaintext(true).build();
            EdgeMessageServiceGrpc.EdgeMessageServiceStub stub = EdgeMessageServiceGrpc.newStub(channel);

            EdgeMessageServiceOuterClass.MeasurementPayload request = EdgeMessageServiceOuterClass.MeasurementPayload.newBuilder()
                    .setTimestamp(m.getTimestamp())
                    .setValue(m.getValue())
                    .setId(m.getId())
                    .setType(m.getType()).build();

            stub.sendMeasurement(request, new StreamObserver<EdgeMessageServiceOuterClass.Empty>() {
                @Override
                public void onNext(EdgeMessageServiceOuterClass.Empty empty) {
                    System.out.println("Misurazione Inviata a\n" + ref_node);
                    onCompleted();
                }

                @Override
                public void onError(Throwable throwable) {
                    System.out.println("Errore nella comunicazione della misurazione. Richiesto nuovo nodo");
                    askThread.askNearNode();
                    onCompleted();
                }

                @Override
                public void onCompleted() {
                    channel.shutdown();
                }
            });
        }



    }

    private class NearNodeThread extends Thread{
        String server_url;
        Node_reference node_ref;
        public NearNodeThread(String srv_url, Node_reference nr){
            server_url = srv_url;
            node_ref=nr;
        }

        public void run(){
            while(true) {
                askNearNode();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

        public void askNearNode(){
            Client cl = Client.create();
            WebResource resource=  cl.resource(rest_url+"server/node/"+position.getX()+"/"+position.getY());
            ClientResponse response = resource.accept("application/json").get(ClientResponse.class);
            Node_reference n = null;

            if(response.getStatus() == 200) {
                n = response.getEntity(Node_reference.class);
                node=n;
            }

            System.out.println("Asked for near node. Talking to "+n);

            synchronized(lock){
                node_ref=n;
            }
        }

    }
}
