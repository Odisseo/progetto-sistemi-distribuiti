package mains;
import util.JavaProcess;

import java.io.IOException;
import java.util.ArrayList;

public class StartSensors {

    static ArrayList<StartSensor> ls  =new ArrayList<>();

    public static void main(String []arg ) {
        for(int i= 0 ; i <10; i++)
            ls.add(new StartSensor());

        for(StartSensor x: ls) {
            x.start();
        }
        System.out.println("tutti i processi sono partiti");
    }

    public static class StartSensor extends Thread{
        JavaProcess process;

        public StartSensor(){
            process = new JavaProcess( "C:\\Program Files\\Java\\jdk-10.0.1");
        }



        public void run(){
            try {
                process.exec(SensorMain.class);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String output = process.getOutput();
            System.out.println(output);
        }



    }

}
