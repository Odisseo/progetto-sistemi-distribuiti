package services;

import beans.ComputedData;
import beans.Node_reference_list;
import beans.Statistic;
import util.Grid;
import util.ServerDataCollector;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("data")
public class AnalystService {

    @Path("nodes")
    @GET
    @Produces({"application/json"})
    public Response getAllnodes(){
        return Response.ok(new Node_reference_list(Grid.getGrid().safeGetAllNodes())).build();
    }


    @Path("stat/node/{id}/{n}")
    @GET
    @Produces({"application/json"})
    public Response getStatsNode(@PathParam("id") int id, @PathParam("n") int n){
        System.out.println(">>"+id+" "+n);

        return ServerDataCollector.getCollector().getLocalData(id,n);
    }
    @Path("stat/city/{n}")
    @GET
    @Produces({"application/json"})
    public Response getStatsCity( @PathParam("n") int n){


        return ServerDataCollector.getServerDataCollector().responseCityMean(n);
    }

    @Path("stat/node/{id}")
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addStatNode(List<Statistic> s){
        System.out.println("Inserimento di " + s.size() + " campionamenti del nodo " + s.get(0).getNode_id());
        ServerDataCollector.getCollector().addLocalData(new ArrayList<>(s));
        return Response.status(200).build();
    }

    @Path("stat/city")
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addGlobalStat( Statistic s){
        System.out.println("Aggiunta statistica globale");
        ServerDataCollector.getServerDataCollector().addGlobalStat(s);
        return Response.ok().build();
    }

    @Path("meansd/node/{id}/{n}")
    @GET
    @Produces({"application/json"})
    public Response getMeanNode(@PathParam("id") int id, @PathParam("n") int n){
        ComputedData computedData = ServerDataCollector.getServerDataCollector().meanAndSdFromNode(id,n);
        if(computedData==null)
            return Response.status(404).build();
        return Response.ok(computedData).build();
    }

    @Path("meansd/city/{n}")
    @GET
    @Produces({"application/json"})
    public Response getMeanCity( @PathParam("n") int n){
        ComputedData computedData = ServerDataCollector.getServerDataCollector().meanAndSdFromCity(n);
        if(computedData==null)
            Response.status(404).build();
        return Response.ok(computedData).build();
    }
}
