package services;

import beans.Node_reference;
import beans.Position;
import util.Grid;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("server")
public class CloudServerService {


    @Path("node")
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response insertNode(Node_reference n){
        System.out.println("insert request from node "+n.getId());
        Response r = Grid.getGrid().insertNode(n);
        System.out.println("Sending Response to "+n.getId());
        return r;
    }


    @Path("node/{x}/{y}")
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response insertNodeInPosition(@PathParam("x") int x,@PathParam("y") int y,  Node_reference n){
        Response r = Response.status(404).build(); //search for a better code
        if(n.getPosition().getX()==x && n.getPosition().getY()==y){
            System.out.println("insert request from node "+n.getId());
            r = Grid.getGrid().insertNode(n);
            System.out.println("Sending Response to "+n.getId());
        }
        return r;
    }



    @Path("node/{x}/{y}")
    @GET
    @Produces({"application/json"})
    public Response getNearestNode(@PathParam("x") int x, @PathParam("y") int y){
        return Grid.getGrid().getNearestNodeCom_ref(new Position(x,y));
    }

    @Path("node/{id}")
    @DELETE
    public Response deleteNode(@PathParam("id") int id){
        boolean done = Grid.getGrid().deleteNode(id);
        if(done)
            return Response.ok().build();
        return Response.status(404).build();
    }


}
