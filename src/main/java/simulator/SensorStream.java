package simulator;

import beans.Measurement;

public interface SensorStream {

    void sendMeasurement(Measurement m);

}
