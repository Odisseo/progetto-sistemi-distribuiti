package util;

import beans.Statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataCollector {
    HashMap<Integer, ArrayList<Statistic>> data;
    Object data_lock;

    public DataCollector(){
        data = new HashMap<>();
        data_lock=new Object();

    }

    public boolean isDataPresent(){
        HashMap<Integer,ArrayList<Statistic>> dt = getSafeData();
        return dt.size()>0;
    }

    public HashMap<Integer,ArrayList<Statistic>> getSafeData(){
        HashMap<Integer,ArrayList<Statistic>> dt = new HashMap<>();
        synchronized (data_lock){
            dt.putAll(data);
        }
        return  dt;
    }

    public void addValueTo( Statistic s){
        synchronized (data_lock){
            if(!data.containsKey(s.getNode_id())){
                data.put(s.getNode_id(), new ArrayList<Statistic>());
            }
            data.get(s.getNode_id()).add(s);
        }
    }

    public void clearValues(Integer id){
        synchronized (data_lock){
            data.get(id).clear();
        }
    }
    public void clearValues(Integer id, int to){
        synchronized (data_lock){
            ArrayList<Statistic> temp = new ArrayList<>(data.get(id).subList(to,data.get(id).size()));
            data.get(id).clear();
            data.get(id).addAll(temp);
        }
    }

    public void removeElements(Integer id, List<Statistic> ls){
        synchronized (data_lock){
            data.get(id).removeAll(ls);
        }
    }

    public ArrayList<Statistic> getValuesOf(Integer x){
        return getSafeData().get(x);
    }

    public void addValuesTo(ArrayList<Statistic> ls){
        Integer id = ls.get(0).getNode_id();
        synchronized (data_lock){
            if(!data.containsKey(id))
                data.put(id,new ArrayList<Statistic>());
            data.get(id).addAll(ls);
        }
    }

}
