package util;

import beans.Node_reference;
import beans.Node_reference_list;
import beans.Position;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.LinkedList;

public class Grid {

    LinkedList<Node_reference> node_list = new LinkedList<>();

    private static Grid grid;
    private Object grid_lock=new Object();

    private Grid(){}

    public static synchronized Grid getGrid(){
        if(grid==null)
            grid=new Grid();
        return grid;

    }

    public boolean tryAddNewNode(Node_reference n){
        synchronized (grid_lock) {
            for (Node_reference r : node_list) {
                if (n.getId() == r.getId() || r.getPosition() == n.getPosition() || n.getPosition().getDistance(r.getPosition()) < 20)
                    return false;
            }
            node_list.add(n);
            return true;
        }

    }

    public Response insertNode(Node_reference n){
        boolean valid = tryAddNewNode(n);
        if (!valid)
            return Response.notModified("Nodo Inaccettabile").build();
        return Response.ok(new Node_reference_list(safeGetAllNodes())).build();

    }


    public Response getNearestNodeCom_ref(Position p){
        int candidate_index=-1;
        int candidate_score=Integer.MAX_VALUE; //no need of it thanks to lazy eval, but intellij complains for initialization

        synchronized (grid_lock) {
            if (node_list.size() == 0)
                return Response.status(404).build();

            for (int i = 0; i < node_list.size(); i++) {
                int score = p.getDistance(node_list.get(i).getPosition());
                if (candidate_index == -1 || score < candidate_score) {
                    candidate_score = score;
                    candidate_index = i;
                }
            }
            if (candidate_index == -1)
                return Response.status(Response.Status.NOT_FOUND).build();
            return Response.ok(node_list.get(candidate_index)).build();
        }

    }

    public boolean deleteNode(int id){
        int remove_idx=-1;
        synchronized (grid_lock) {
            for (Node_reference x : node_list) {
                if (x.getId() == id) {
                    remove_idx = node_list.indexOf(x);
                    break;
                }
            }
            if (remove_idx != -1) {
                node_list.remove(remove_idx);
                return true;
            }
            return false;
        }
    }

    public synchronized ArrayList<Node_reference> safeGetAllNodes(){
        return new ArrayList<>(node_list);
    }


}
