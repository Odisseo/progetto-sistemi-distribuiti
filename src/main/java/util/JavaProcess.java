package util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public final class JavaProcess {
    private String output = "";
    private String javaHome = "";

    public JavaProcess(String javaHome) {
        this.javaHome = javaHome;
    }

    public int exec(Class klass) throws IOException, InterruptedException {
        String javaBin = javaHome +
                File.separator + "bin" +
                File.separator + "java";
        String classpath = System.getProperty("java.class.path");
        String className = klass.getCanonicalName();

        ProcessBuilder builder = new ProcessBuilder(
                javaBin, "-cp", classpath, className);
        builder.redirectErrorStream();

        Process process = builder.start();

        // Read and process the command output
        readOutput(process.getInputStream());

        process.waitFor();

        return process.exitValue();
    }

    /**
     * Read and process the command output, doesn't include error.
     *
     * @param input
     */
    private void readOutput(final InputStream input) {
        new Thread(new Runnable() {
            public void run() {
                StringBuilder sb = new StringBuilder();
                int data;
                try {
                    while((data = input.read()) != -1) {
                        sb.append((char)data);
                    }

                    output = sb.toString();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}