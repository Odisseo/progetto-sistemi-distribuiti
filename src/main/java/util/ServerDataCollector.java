package util;

import beans.ComputedData;
import beans.Statistic;
import beans.Statistic_list;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;

public class ServerDataCollector {

    private ArrayList<Statistic> city_data;
    private DataCollector local_data;
    private Object city_lock;
    private Object local_lock;

    private static ServerDataCollector collector;

    private ServerDataCollector(){
        city_lock=new Object();
        local_lock=new Object();
        local_data = new DataCollector();
        city_data=new ArrayList<>();

    }

    public static synchronized ServerDataCollector getServerDataCollector(){
        if(collector==null)
            collector=new ServerDataCollector();
        return collector;
    }


    public Response addGlobalStat(Statistic s){
        synchronized (city_lock){
            city_data.add(s);
        }

        return Response.ok().build();
    }

    public void addLocalData(ArrayList<Statistic> ls){
        local_data.addValuesTo(ls);

    }
    public Response getLocalData(int id, int n){

        ArrayList<Statistic> ls = local_data.getValuesOf(id);
        if(ls==null)
            return Response.status(Response.Status.NOT_FOUND).build();
        if(ls.size()<n)
            return Response.ok(ls).build();
        return Response.ok(new Statistic_list(new ArrayList<>(ls.subList(ls.size()-n,ls.size())))).build();

    }

    public Response responseCityMean(int n) {
        ArrayList<Statistic> ls;
        synchronized (city_lock){
           ls = new ArrayList<>(city_data);
        }
        ArrayList toRet;
        if(ls.size()-n<0)
            toRet=ls;
        else
            toRet= new ArrayList(ls.subList(ls.size()-n,ls.size()));


        return Response.ok(new Statistic_list(toRet)).build();
    }


    public ArrayList<Statistic> getCity_data() {
        synchronized (city_lock) {
            return new ArrayList<>(city_data);
        }
    }

    public void setCity_data(ArrayList<Statistic> city_data) {
        this.city_data = city_data;
    }

    public Object getCity_lock() {
        return city_lock;
    }

    public void setCity_lock(Object city_lock) {
        this.city_lock = city_lock;
    }

    public static ServerDataCollector getCollector() {
        return collector;
    }


    public static void setCollector(ServerDataCollector collector) {
        ServerDataCollector.collector = collector;
    }


    public ComputedData meanAndSd(ArrayList<Statistic> ls){
        double mean = 0;
        for(Statistic x: ls)
            mean+=x.getValue();
        mean/=ls.size();

        double standardDeviation=0;
        for(Statistic x: ls){
            standardDeviation+=Math.pow((x.getValue()-mean),2);
        }
        standardDeviation = Math.sqrt((standardDeviation/ls.size()));
        return new ComputedData(mean,standardDeviation);
    }

    public ComputedData meanAndSdFromNode(int id, int n){
        ComputedData ret = null;
        HashMap<Integer, ArrayList<Statistic>> dt = local_data.getSafeData();
        if(dt.containsKey(id)){
            ArrayList<Statistic> ls;
            if(dt.get(id).size()-n<0)
                ls = dt.get(id);
            else
                ls = new ArrayList<>(dt.get(id).subList(dt.get(id).size()-n,dt.get(id).size()));
            ret = meanAndSd(ls);

        }
        return ret;
    }

    public ComputedData meanAndSdFromCity( int n){
        ComputedData ret = null;
        ArrayList<Statistic> city = getCity_data();
        ArrayList<Statistic> ls;
        if(city.size()-n<0)
            ls = city;
        else
            ls = new ArrayList<>(city.subList(city.size()-n,city.size()));
        return meanAndSd(ls);
    }


}
