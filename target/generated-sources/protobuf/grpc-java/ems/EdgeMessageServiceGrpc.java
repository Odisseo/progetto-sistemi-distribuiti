package ems;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.7.0)",
    comments = "Source: EdgeMessageService.proto")
public final class EdgeMessageServiceGrpc {

  private EdgeMessageServiceGrpc() {}

  public static final String SERVICE_NAME = "ems.EdgeMessageService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.ProvaMessaggio,
      ems.EdgeMessageServiceOuterClass.ProvaMessaggio> METHOD_PROVA =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.ProvaMessaggio, ems.EdgeMessageServiceOuterClass.ProvaMessaggio>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "prova"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.ProvaMessaggio.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.ProvaMessaggio.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("prova"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.CallingCard,
      ems.EdgeMessageServiceOuterClass.WhoIsCoordinator> METHOD_REQUEST_COORDINATOR =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.CallingCard, ems.EdgeMessageServiceOuterClass.WhoIsCoordinator>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "requestCoordinator"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.CallingCard.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.WhoIsCoordinator.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("requestCoordinator"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.MeasurementPayload,
      ems.EdgeMessageServiceOuterClass.Empty> METHOD_SEND_MEASUREMENT =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.MeasurementPayload, ems.EdgeMessageServiceOuterClass.Empty>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "sendMeasurement"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.MeasurementPayload.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.Empty.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("sendMeasurement"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.NodeMean,
      ems.EdgeMessageServiceOuterClass.TimeMean> METHOD_MEAN_TO_COORDINATOR =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.NodeMean, ems.EdgeMessageServiceOuterClass.TimeMean>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "meanToCoordinator"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.NodeMean.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.TimeMean.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("meanToCoordinator"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator,
      ems.EdgeMessageServiceOuterClass.Empty> METHOD_SEND_COORDINATOR =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator, ems.EdgeMessageServiceOuterClass.Empty>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "sendCoordinator"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.WhoIsCoordinator.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.Empty.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("sendCoordinator"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.ElectionPayload,
      ems.EdgeMessageServiceOuterClass.ElectionOk> METHOD_START_ELECTION =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.ElectionPayload, ems.EdgeMessageServiceOuterClass.ElectionOk>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "startElection"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.ElectionPayload.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.ElectionOk.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("startElection"))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator,
      ems.EdgeMessageServiceOuterClass.ElectionOk> METHOD_WON_ELECTION =
      io.grpc.MethodDescriptor.<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator, ems.EdgeMessageServiceOuterClass.ElectionOk>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ems.EdgeMessageService", "wonElection"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.WhoIsCoordinator.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ems.EdgeMessageServiceOuterClass.ElectionOk.getDefaultInstance()))
          .setSchemaDescriptor(new EdgeMessageServiceMethodDescriptorSupplier("wonElection"))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static EdgeMessageServiceStub newStub(io.grpc.Channel channel) {
    return new EdgeMessageServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static EdgeMessageServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new EdgeMessageServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static EdgeMessageServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new EdgeMessageServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class EdgeMessageServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void prova(ems.EdgeMessageServiceOuterClass.ProvaMessaggio request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ProvaMessaggio> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_PROVA, responseObserver);
    }

    /**
     */
    public void requestCoordinator(ems.EdgeMessageServiceOuterClass.CallingCard request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_REQUEST_COORDINATOR, responseObserver);
    }

    /**
     */
    public void sendMeasurement(ems.EdgeMessageServiceOuterClass.MeasurementPayload request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SEND_MEASUREMENT, responseObserver);
    }

    /**
     */
    public void meanToCoordinator(ems.EdgeMessageServiceOuterClass.NodeMean request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.TimeMean> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_MEAN_TO_COORDINATOR, responseObserver);
    }

    /**
     */
    public void sendCoordinator(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SEND_COORDINATOR, responseObserver);
    }

    /**
     */
    public void startElection(ems.EdgeMessageServiceOuterClass.ElectionPayload request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_START_ELECTION, responseObserver);
    }

    /**
     */
    public void wonElection(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_WON_ELECTION, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_PROVA,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.ProvaMessaggio,
                ems.EdgeMessageServiceOuterClass.ProvaMessaggio>(
                  this, METHODID_PROVA)))
          .addMethod(
            METHOD_REQUEST_COORDINATOR,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.CallingCard,
                ems.EdgeMessageServiceOuterClass.WhoIsCoordinator>(
                  this, METHODID_REQUEST_COORDINATOR)))
          .addMethod(
            METHOD_SEND_MEASUREMENT,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.MeasurementPayload,
                ems.EdgeMessageServiceOuterClass.Empty>(
                  this, METHODID_SEND_MEASUREMENT)))
          .addMethod(
            METHOD_MEAN_TO_COORDINATOR,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.NodeMean,
                ems.EdgeMessageServiceOuterClass.TimeMean>(
                  this, METHODID_MEAN_TO_COORDINATOR)))
          .addMethod(
            METHOD_SEND_COORDINATOR,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.WhoIsCoordinator,
                ems.EdgeMessageServiceOuterClass.Empty>(
                  this, METHODID_SEND_COORDINATOR)))
          .addMethod(
            METHOD_START_ELECTION,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.ElectionPayload,
                ems.EdgeMessageServiceOuterClass.ElectionOk>(
                  this, METHODID_START_ELECTION)))
          .addMethod(
            METHOD_WON_ELECTION,
            asyncUnaryCall(
              new MethodHandlers<
                ems.EdgeMessageServiceOuterClass.WhoIsCoordinator,
                ems.EdgeMessageServiceOuterClass.ElectionOk>(
                  this, METHODID_WON_ELECTION)))
          .build();
    }
  }

  /**
   */
  public static final class EdgeMessageServiceStub extends io.grpc.stub.AbstractStub<EdgeMessageServiceStub> {
    private EdgeMessageServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private EdgeMessageServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EdgeMessageServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new EdgeMessageServiceStub(channel, callOptions);
    }

    /**
     */
    public void prova(ems.EdgeMessageServiceOuterClass.ProvaMessaggio request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ProvaMessaggio> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_PROVA, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void requestCoordinator(ems.EdgeMessageServiceOuterClass.CallingCard request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_REQUEST_COORDINATOR, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void sendMeasurement(ems.EdgeMessageServiceOuterClass.MeasurementPayload request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SEND_MEASUREMENT, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void meanToCoordinator(ems.EdgeMessageServiceOuterClass.NodeMean request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.TimeMean> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_MEAN_TO_COORDINATOR, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void sendCoordinator(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SEND_COORDINATOR, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void startElection(ems.EdgeMessageServiceOuterClass.ElectionPayload request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_START_ELECTION, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void wonElection(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request,
        io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_WON_ELECTION, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class EdgeMessageServiceBlockingStub extends io.grpc.stub.AbstractStub<EdgeMessageServiceBlockingStub> {
    private EdgeMessageServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private EdgeMessageServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EdgeMessageServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new EdgeMessageServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.ProvaMessaggio prova(ems.EdgeMessageServiceOuterClass.ProvaMessaggio request) {
      return blockingUnaryCall(
          getChannel(), METHOD_PROVA, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.WhoIsCoordinator requestCoordinator(ems.EdgeMessageServiceOuterClass.CallingCard request) {
      return blockingUnaryCall(
          getChannel(), METHOD_REQUEST_COORDINATOR, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.Empty sendMeasurement(ems.EdgeMessageServiceOuterClass.MeasurementPayload request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SEND_MEASUREMENT, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.TimeMean meanToCoordinator(ems.EdgeMessageServiceOuterClass.NodeMean request) {
      return blockingUnaryCall(
          getChannel(), METHOD_MEAN_TO_COORDINATOR, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.Empty sendCoordinator(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SEND_COORDINATOR, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.ElectionOk startElection(ems.EdgeMessageServiceOuterClass.ElectionPayload request) {
      return blockingUnaryCall(
          getChannel(), METHOD_START_ELECTION, getCallOptions(), request);
    }

    /**
     */
    public ems.EdgeMessageServiceOuterClass.ElectionOk wonElection(ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request) {
      return blockingUnaryCall(
          getChannel(), METHOD_WON_ELECTION, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class EdgeMessageServiceFutureStub extends io.grpc.stub.AbstractStub<EdgeMessageServiceFutureStub> {
    private EdgeMessageServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private EdgeMessageServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EdgeMessageServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new EdgeMessageServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.ProvaMessaggio> prova(
        ems.EdgeMessageServiceOuterClass.ProvaMessaggio request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_PROVA, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator> requestCoordinator(
        ems.EdgeMessageServiceOuterClass.CallingCard request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_REQUEST_COORDINATOR, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.Empty> sendMeasurement(
        ems.EdgeMessageServiceOuterClass.MeasurementPayload request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SEND_MEASUREMENT, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.TimeMean> meanToCoordinator(
        ems.EdgeMessageServiceOuterClass.NodeMean request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_MEAN_TO_COORDINATOR, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.Empty> sendCoordinator(
        ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SEND_COORDINATOR, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.ElectionOk> startElection(
        ems.EdgeMessageServiceOuterClass.ElectionPayload request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_START_ELECTION, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ems.EdgeMessageServiceOuterClass.ElectionOk> wonElection(
        ems.EdgeMessageServiceOuterClass.WhoIsCoordinator request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_WON_ELECTION, getCallOptions()), request);
    }
  }

  private static final int METHODID_PROVA = 0;
  private static final int METHODID_REQUEST_COORDINATOR = 1;
  private static final int METHODID_SEND_MEASUREMENT = 2;
  private static final int METHODID_MEAN_TO_COORDINATOR = 3;
  private static final int METHODID_SEND_COORDINATOR = 4;
  private static final int METHODID_START_ELECTION = 5;
  private static final int METHODID_WON_ELECTION = 6;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final EdgeMessageServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(EdgeMessageServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PROVA:
          serviceImpl.prova((ems.EdgeMessageServiceOuterClass.ProvaMessaggio) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ProvaMessaggio>) responseObserver);
          break;
        case METHODID_REQUEST_COORDINATOR:
          serviceImpl.requestCoordinator((ems.EdgeMessageServiceOuterClass.CallingCard) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.WhoIsCoordinator>) responseObserver);
          break;
        case METHODID_SEND_MEASUREMENT:
          serviceImpl.sendMeasurement((ems.EdgeMessageServiceOuterClass.MeasurementPayload) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty>) responseObserver);
          break;
        case METHODID_MEAN_TO_COORDINATOR:
          serviceImpl.meanToCoordinator((ems.EdgeMessageServiceOuterClass.NodeMean) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.TimeMean>) responseObserver);
          break;
        case METHODID_SEND_COORDINATOR:
          serviceImpl.sendCoordinator((ems.EdgeMessageServiceOuterClass.WhoIsCoordinator) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.Empty>) responseObserver);
          break;
        case METHODID_START_ELECTION:
          serviceImpl.startElection((ems.EdgeMessageServiceOuterClass.ElectionPayload) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk>) responseObserver);
          break;
        case METHODID_WON_ELECTION:
          serviceImpl.wonElection((ems.EdgeMessageServiceOuterClass.WhoIsCoordinator) request,
              (io.grpc.stub.StreamObserver<ems.EdgeMessageServiceOuterClass.ElectionOk>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class EdgeMessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    EdgeMessageServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ems.EdgeMessageServiceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("EdgeMessageService");
    }
  }

  private static final class EdgeMessageServiceFileDescriptorSupplier
      extends EdgeMessageServiceBaseDescriptorSupplier {
    EdgeMessageServiceFileDescriptorSupplier() {}
  }

  private static final class EdgeMessageServiceMethodDescriptorSupplier
      extends EdgeMessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    EdgeMessageServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (EdgeMessageServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new EdgeMessageServiceFileDescriptorSupplier())
              .addMethod(METHOD_PROVA)
              .addMethod(METHOD_REQUEST_COORDINATOR)
              .addMethod(METHOD_SEND_MEASUREMENT)
              .addMethod(METHOD_MEAN_TO_COORDINATOR)
              .addMethod(METHOD_SEND_COORDINATOR)
              .addMethod(METHOD_START_ELECTION)
              .addMethod(METHOD_WON_ELECTION)
              .build();
        }
      }
    }
    return result;
  }
}
